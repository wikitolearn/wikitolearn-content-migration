#!/bin/bash
# HELP setup the system
set -e
set -x
cd `dirname "${BASH_SOURCE[0]}"`/..

if [[ "$1" != "services-only" ]]
then
  if ! docker container inspect wikitolearn-migration-keycloak-db &> /dev/null
  then
    docker volume create \
      --label wikitolearn-migration=1 \
      --label wikitolearn-migration-keycloak=1 \
      wikitolearn-migration-keycloak-db
    docker container create \
      --name wikitolearn-migration-keycloak-db \
      --hostname wikitolearn-migration-keycloak-db.lan \
      --label wikitolearn-migration=1 \
      --label wikitolearn-migration-keycloak=1 \
      -ti \
      -e MYSQL_DATABASE=keycloak \
      -e MYSQL_USER=keycloak \
      -e MYSQL_PASSWORD=password \
      -e MYSQL_ROOT_PASSWORD=root \
      -v wikitolearn-migration-keycloak-db:/var/lib/mysql \
      mariadb:10.1
  fi
  if ! docker container inspect wikitolearn-migration-keycloak-phpmyadmin &> /dev/null
  then
    docker volume create \
      --label wikitolearn-migration=1 \
      --label wikitolearn-migration-keycloak=1 \
      wikitolearn-migration-keycloak-phpmyadmin
    docker container create \
      --name wikitolearn-migration-keycloak-phpmyadmin \
      --hostname wikitolearn-migration-keycloak-phpmyadmin.lan \
      --label wikitolearn-migration=1 \
      --label wikitolearn-migration-keycloak=1 \
      -ti \
      -e PMA_USER=root \
      -e PMA_PASSWORD=root \
      -v wikitolearn-migration-keycloak-phpmyadmin:/sessions \
      --link wikitolearn-migration-keycloak-db:db \
      -p 9081:80 \
      phpmyadmin/phpmyadmin
  fi
  if ! docker container inspect wikitolearn-migration-keycloak &> /dev/null
  then
    docker container create \
      --name wikitolearn-migration-keycloak \
      --hostname wikitolearn-migration-keycloak.lan \
      --label wikitolearn-migration=1 \
      --label wikitolearn-migration-keycloak=1 \
      -e KEYCLOAK_USER=admin \
      -e KEYCLOAK_PASSWORD=admin \
      -e DB_ADDR=mysql \
      -e MYSQL_PORT=3306 \
      -e DB_DATABASE=keycloak \
      -e DB_USER=keycloak \
      -e DB_PASSWORD=password \
      -e DB_VENDOR=mariadb \
      --restart on-failure \
      --link wikitolearn-migration-keycloak-db:mysql \
      -p 9080:9080 \
      jboss/keycloak:4.5.0.Final \
      -b 0.0.0.0 \
      -Djboss.socket.binding.port-offset=1000
  fi
  if [[ $(docker container inspect -f '{{ .State.Status }}' wikitolearn-migration-keycloak-db) != "running" ]]
  then
    docker container start wikitolearn-migration-keycloak-db
  fi
  if [[ $(docker container inspect -f '{{ .State.Status }}' wikitolearn-migration-keycloak-phpmyadmin) != "running" ]]
  then
    docker container start wikitolearn-migration-keycloak-phpmyadmin
  fi
  if [[ $(docker container inspect -f '{{ .State.Status }}' wikitolearn-migration-keycloak) != "running" ]]
  then
    docker container start wikitolearn-migration-keycloak
  fi


  if ! docker container inspect wikitolearn-migration-source &> /dev/null
  then
    docker volume create \
      --label wikitolearn-migration=1 \
      --label wikitolearn-migration-source=1 \
      wikitolearn-migration-source
    docker container create \
      --name wikitolearn-migration-source \
      --hostname wikitolearn-migration-source.lan \
      --label wikitolearn-migration=1 \
      --label wikitolearn-migration-source=1 \
      -ti \
      -e MYSQL_ROOT_PASSWORD=root \
      -v wikitolearn-migration-source:/var/lib/mysql \
      mariadb:10.1
    TMP_CONFIG=`mktemp -d`
    cat <<EOF > $TMP_CONFIG/performance.cnf
[mysqld]
innodb_read_io_threads=16
innodb_write_io_threads=16
innodb_buffer_pool_size=8G
innodb_buffer_pool_load_at_startup=ON
innodb_log_file_size = 4GB
innodb_log_files_in_group=2
innodb_file_per_table=1
innodb_log_buffer_size=8M
innodb_flush_method=nosync
innodb_flush_log_at_trx_commit=0
skip-innodb_doublewrite

max_connections = 18
query_cache_size = 8GB
wait_timeout = 86400

innodb_lock_wait_timeout=1

skip-name-resolve

EOF
    docker container cp $TMP_CONFIG/performance.cnf wikitolearn-migration-source:/etc/mysql/conf.d
    rm $TMP_CONFIG -rf
  fi
  if ! docker container inspect wikitolearn-migration-source-phpmyadmin &> /dev/null
  then
    docker volume create \
      --label wikitolearn-migration=1 \
      --label wikitolearn-migration-source=1 \
      wikitolearn-migration-source-phpmyadmin
    docker container create \
      --name wikitolearn-migration-source-phpmyadmin \
      --hostname wikitolearn-migration-source-phpmyadmin.lan \
      --label wikitolearn-migration=1 \
      --label wikitolearn-migration-source=1 \
      -ti \
      -e PMA_USER=root \
      -e PMA_PASSWORD=root \
      -v wikitolearn-migration-source-phpmyadmin:/sessions \
      --link wikitolearn-migration-source:db \
      -p 8082:80 \
      phpmyadmin/phpmyadmin
  fi
  if [[ $(docker container inspect -f '{{ .State.Status }}' wikitolearn-migration-source) != "running" ]]
  then
    docker container start wikitolearn-migration-source
  fi
  if [[ $(docker container inspect -f '{{ .State.Status }}' wikitolearn-migration-source-phpmyadmin) != "running" ]]
  then
    docker container start wikitolearn-migration-source-phpmyadmin
  fi



  if ! docker container inspect wikitolearn-migration-destination &> /dev/null
  then
    docker volume create \
      --label wikitolearn-migration=1 \
      --label wikitolearn-migration-destination=1 \
      wikitolearn-migration-destination-configdb
    docker volume create \
      --label wikitolearn-migration=1 \
      --label wikitolearn-migration-destination=1 \
      wikitolearn-migration-destination-db
    docker container create \
      --label wikitolearn-migration=1 \
      --label wikitolearn-migration-destination=1 \
      -ti \
      -v wikitolearn-migration-destination-configdb:/data/configdb \
      -v wikitolearn-migration-destination-db:/data/db \
      --name wikitolearn-migration-destination \
      --hostname wikitolearn-migration-destination.lan \
      mongo:3.4
  fi
  if ! docker container inspect wikitolearn-migration-destination-mongoexpress &> /dev/null
  then
    docker container create \
      --label wikitolearn-migration=1 \
      --label wikitolearn-migration-destination=1 \
      --name wikitolearn-migration-destination-mongoexpress \
      --hostname wikitolearn-migration-destination-mongoexpress.lan \
      -ti \
      -p 8081:8081 \
      --link wikitolearn-migration-destination:mongo \
      mongo-express
  fi
  if [[ $(docker container inspect -f '{{ .State.Status }}' wikitolearn-migration-destination) != "running" ]]
  then
    docker container start wikitolearn-migration-destination
  fi
  if [[ $(docker container inspect -f '{{ .State.Status }}' wikitolearn-migration-destination-mongoexpress) != "running" ]]
  then
    docker container start wikitolearn-migration-destination-mongoexpress
  fi

  # set +x
  echo "Wait for mysql..."
  while ! docker container exec wikitolearn-migration-source mysql -uroot -proot -Bse 'SELECT 1' &> /dev/null
  do
    sleep 1
  done
  for db in $(ls dumps/production-v1-dump/ | sed 's/.struct.sql//g' | sed 's/.data.sql//g' | sort | uniq)
  do
    echo "Test $db"
    if ! docker container exec wikitolearn-migration-source mysql -uroot -proot $db -Bse 'SELECT 1' &> /dev/null
    then
      echo "> create $db"
      docker container exec wikitolearn-migration-source mysql -uroot -proot -Bse "CREATE DATABASE $db"
      echo ">> struct"
      cat dumps/production-v1-dump/$db.struct.sql | docker container exec -i wikitolearn-migration-source mysql -uroot -proot $db
      echo ">> data"
      cat dumps/production-v1-dump/$db.data.sql   | docker container exec -i wikitolearn-migration-source mysql -uroot -proot $db
    fi
  done


  while ! docker exec -i wikitolearn-migration-destination mongo --quiet --eval '{ ping: 1 }'
  do
    sleep 1
  done
  if test -f dumps/caches/parsoid-cache.dump.gz
  then
    cat dumps/caches/parsoid-cache.dump.gz | docker exec \
      -i \
      wikitolearn-migration-destination \
      mongorestore \
      --archive \
      --gzip \
      --drop
  fi
  if test -f dumps/caches/formulas.dump.gz
  then
    cat dumps/caches/formulas.dump.gz | docker exec \
      -i \
      wikitolearn-migration-destination \
      mongorestore \
      --archive \
      --gzip \
      --drop
  fi
fi


export DOCKER_MONGO_HOST=$(docker container inspect wikitolearn-migration-destination --format '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}')

if test ! -d storage/
then
  mkdir storage/
fi
cd storage/

for subdir in courses chapters pages coursessecurity
do
  while test ! -d $subdir/
  do
    if ! git clone kde:wikitolearn-$subdir-backend $subdir/
    then
      if ! git clone kde:scratch/atundo/wikitolearn-$subdir-backend $subdir/
      then
        if ! git clone kde:scratch/tomaluca/wikitolearn-$subdir-backend $subdir/
        then
          sleep 10
        fi
      fi
    fi
  done
  cd $subdir/
  if [[ $(git remote -v | wc -l) -gt 0 ]]
  then
    while ! git pull
    do
      sleep 1
    done
  fi
  . .env
  docker-compose -f docker-compose.yml build
  docker container kill \
    wikitolearn-service-${subdir}-backend || true
  docker container rm \
    wikitolearn-service-${subdir}-backend || true
  docker container run \
    -tid \
    --label wikitolearn-migration=1 \
    --name wikitolearn-service-${subdir}-backend \
    -e MONGO_HOST=$DOCKER_MONGO_HOST \
    -p $SERVICE_PORT:$SERVICE_PORT \
    wikitolearn/${subdir}-backend
  cd ..
done
