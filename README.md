# WikiToLearn Content Migration

## bin/ content
* `wtlm-cache-export.sh` export caches (parsoid and math-rendering example)
* `wtlm-clean-destination.sh` deletes only destination databases
* `wtlm-clean.sh` delete verything
* `wtlm-down.sh` shutdown the system
* `wtlm-output-export.sh` export destination mw pages collections
* `wtlm-run.sh` run the pipeline
* `wtlm-setup.sh` setup the system
* `wtlm-ship.sh` run a little webui to test the data
* `wtlm-update-production-v1-dump.sh` download the data from a backup NAS

## pipeline scripts
* `005-setup-source`
 * `005-wtl1-logging.py` 
 * `010-wtl1-unified-revision.py` 
 * `015-wtl1-superlog.py` 
* `010-setup-aux-db`
 * `005-create-index.py` 
* `015-import-pages-log`
 * `005-create-pages.py` 
 * `010-create-pages-with-resused-ids.py` 
 * `015-set-validity-range.py` 
 * `020-superlog-load-base.py` 
 * `025-delete-pages-with-wrong-ns.py` 
 * `030-superlog-delete-with-last-operation-delete-before-day-zero.py` 
 * `035-set-CourseRoot.py` 
* `020-fix-data`
 * `005-superlog-fix-params.py` 
 * `010-superlog-sort-by-timestamp.py` 
 * `015-superlog-fix-delete-actions.py` 
 * `020-superlog-fix-titles-with-move-events.py` 
 * `025-superlog-remove-items-before-day-zero.py` 
 * `030-superlog-load-text.py` 
 * `035-superlog-add-text-to-events.py` 
 * `040-superlog-set-timestamp-next.py` 
 * `045-delete-page-with-last-operation-a-delete.py` 
* `025-parse-struct-pages`
 * `005-CourseRoot.py` 
 * `010-CourseMetadata.py` 
 * `015-CourseLevelTwo.py` 
 * `020-CourseLevelThree.py` 
 * `025-delete-page-not-in-use.py` 
* `030-parse-content`
 * `005-parse-content.py` 
 * `010-delete-redirects.py` 
* `035-fix-parsoid-cache`
 * `005-fix-parsoid-cache.py` 
* `040-fix-superlog`
 * `005-create-the-id-field-id-in-subpage-log.py` 
 * `010-insert-page-creation.py` 
 * `015-fix-all-used-titles.py` 
 * `020-all-fix-users-ids.py` 
* `045-content-parsing`
 * `005-text-extract-objects.py` 
 * `010-resolve-links.py` 
 * `015-resolve-math.py` 
* `050-math-resolve`
 * `005-texvcjs.js` 
 * `010-render.py` 
 * `015-lookup-pages.py` 
* `055-file-resolve`
 * `005-load-files-revisions.py` 
 * `010-download.py` 
* `060-mw-test-online`
 * `005-check-head-online-structure.py` 
 * `010-check-head-online-wikitext.py` 
 * `015-missing-pages.py` 
* `065-import-users-from-mw`
 * `005-import-user.py` 
 * `010-test-users-with-B-passwords.py` 
 * `015-prepare-keycloak-object.py` 
* `070-export-users-to-keycloak`
 * `005-keycloak-setup.py` 
 * `010-upload-in-keycloak.py` 
 * `015-anon-keycloak.py` 
 * `020-keycloak-users-id-association.py` 
* `075-create-wtl-pages`
 * `005-create-wtl-pages.py` 
 * `010-fix-titles-CourseLevelThree-and-CourseLevelTwo.py` 
 * `015-fix-titles-CourseRoot.py` 
 * `020-lookup-owner-CourseRoot.py` 
 * `025-remove-duplicates.py` 
* `080-add-revisions`
 * `005-create-additional-revisions.py` 
* `085-eve-export`
 * `005-prepare-to-eve.py` 
 * `010-pages.py` 
 * `015-chapters-courses.py` 
 * `020-coursessecurity.py` 
* `090-eve-fix`
 * `005-pages.py` 
 * `010-chapters.py` 
 * `015-courses.py` 
 * `020-coursessecurity.py` 
