from urllib.parse import urlencode, quote_plus, urlparse, unquote
from tqdm import tqdm
from pprint import pprint
from bson.objectid import ObjectId
from bs4 import BeautifulSoup

import time
import yaml
import json
import phpserialize
import sys
import os
import os.path
import glob
import datetime
import pymongo
import re
import MySQLdb
import subprocess
import requests
import threading
import concurrent.futures
import functools
import traceback
import pickle
import copy
import hashlib
import logging
import logging.handlers
import multiprocessing
import codecs

def wrapper_default_user_agent(func):
    def replaced_fn(name='WikiToLearn Migration Tool (python3 requests)'):
        return name

    def authenticate_and_call(*args, **kwargs):
        return replaced_fn(*args, **kwargs)
    return authenticate_and_call
requests.utils.default_user_agent = wrapper_default_user_agent(requests.utils.default_user_agent)


# fix for utf-8 output
sys.stdout = open(sys.stdout.fileno(), mode='w', encoding='utf8', buffering=1)

# 3 nov 2016, 1.0 release
day_zero =  datetime.datetime(2016, 11, 3, 00, 00)

# keycloak config
keycloak_realm_id = "wikitolearn-local"
keycloak_realm_username = "admin"
keycloak_realm_password = "admin"
keycloak_requests_verify = False
keycloak_base_url = os.environ.get("KEYCLOAK_URI") + "/auth/"
keycloak_realm_base_url = keycloak_base_url + "admin/realms/{}".format(keycloak_realm_id)

keycloak_auth_obj = None
keycloak_last_login_success = 0
def keycloak_get_auth_headers(base_headers={}):
    global keycloak_auth_obj
    global keycloak_base_url
    global keycloak_last_login_success
    headers = dict(base_headers)
    do_login = False
    if keycloak_auth_obj == None:
        do_login = True
    else:
        if time.time() - keycloak_last_login_success > (keycloak_auth_obj['expires_in'] - 5):
            do_login = True

    if do_login:
        auth_endpoint = keycloak_base_url + "realms/master/"
        data = {}
        data["client_id"] = "admin-cli"
        data["username"] = keycloak_realm_username
        data["password"] = keycloak_realm_password
        data["grant_type"] = "password"

        try:
            auth_r = requests.post(
                auth_endpoint + "protocol/openid-connect/token",
                data=data,
                verify=keycloak_requests_verify
                )
            auth_r.raise_for_status()
            keycloak_auth_obj = auth_r.json()
            keycloak_last_login_success = time.time()
        except Exception as e:
            print("Keycloak Login Exception")
            raise e
    if keycloak_auth_obj != None and 'access_token' in keycloak_auth_obj:
        headers["Authorization"] = "Bearer {}".format(keycloak_auth_obj['access_token'])
    else:
        pprint(keycloak_auth_obj)
        time.sleep(1)
    return headers


data_dir = "/srv/"
os.chdir(data_dir)

config_file = "{}/config.yml".format(data_dir)
session_file = None

if not os.path.isfile(config_file):
    print("File {} does not exist".format(config_file))
    sys.exit(1)

config_file_obj = None
with open(config_file) as fh:
    config_file_obj = yaml.load(fh)
    if not 'wikiuser' in config_file_obj:
        print("Missing wikiuser key in the config file ({})".format(config_file))
        sys.exit(1)
    if not 'username' in config_file_obj['wikiuser']:
        print("Missing wikiuser.username key in the config file ({})".format(config_file))
        sys.exit(1)
    if not 'password' in config_file_obj['wikiuser']:
        print("Missing wikiuser.password key in the config file ({})".format(config_file))
        sys.exit(1)
    fh.close()

lang = os.environ.get('WTL_LANG')
dns_domain = os.environ.get('WTL_DOMAIN')

if not os.path.isdir('/srv/logs/'):
    os.mkdir('/srv/logs/')

logger_name = "{lang} {script}".format(
    script=sys.argv[0][13:],
    lang=lang,
)
logger = logging.getLogger(logger_name)

logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

fh = logging.handlers.TimedRotatingFileHandler('/srv/logs/pipeline.log', backupCount=30, when='midnight', encoding='utf8')
fh.setLevel(logging.DEBUG)
fh.setFormatter(formatter)
logger.addHandler(fh)

ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)
ch.setFormatter(formatter)
logger.addHandler(ch)


destination_link = pymongo.MongoClient('destination', 27017)
destination_db = destination_link[lang]
destination_aux_db = destination_link['migration_shared_data']
source_db = MySQLdb.connect('source', 'root', 'root', lang+"wikitolearn", use_unicode=True, charset="utf8mb4")

restbase_base = "http://restbase.{dns_domain}/{lang}.{dns_domain}/v1/".format(**{
    'lang':lang,
    'dns_domain':dns_domain,
})

from utilmysql import *
from utillookupsubpage import *
from utilparsoidcall import call_parsoid

def parse_mw_text(mw_text):
    mw_text = mw_text.strip()
    if mw_text == "":
        return ""
    mw_hash = hashlib.sha256(mw_text.encode('utf-8')).hexdigest()

    cache = destination_aux_db['parsoid_cache'].find_one({'$or':[
        {'mw_hash':mw_hash},
    ]}, sort=[("mw_hash", 1)])
    if cache == None or not 'parsoid_text' in cache:
        logger.debug("Cache MISS for parsoid {}".format(mw_hash))

        parsoid_output = call_parsoid(lang, dns_domain, mw_text)

        parsoid_output_parsing = str(BeautifulSoup(parsoid_output, 'lxml').prettify())
    else:
        parsoid_output_parsing = cache['parsoid_text']

    if cache == None:
        destination_aux_db['parsoid_cache'].insert_one(
            {
                'mw_hash':mw_hash,
                'mw_text':mw_text,
                'parsoid_text':parsoid_output_parsing,
                'last_update': datetime.datetime.now(),
            }
        )
    else:
        destination_aux_db['parsoid_cache'].update_one({
            '_id':cache['_id'],
        }, {'$set':{
            "mw_text":mw_text,
            "parsoid_text":parsoid_output_parsing,
            'last_update': datetime.datetime.now(),
        }})
    return parsoid_output_parsing


# MW API utils
def login_api(lang, dns_domain):
    session_file = "{}/session.yml".format(data_dir)
    if os.path.isfile(session_file):
        print("File {} exists, login skipped".format(session_file))
    else:
        requests_session = requests.Session()

        # https://www.mediawiki.org/wiki/API:Tokens
        api_get_session_id_payload = {
            "action": "query",
            "meta": "tokens",
            "type": "login",
            "format": "json"
        }
        api_get_session_id_url = "https://{}.{}/api.php".format(lang, dns_domain)
        api_get_session_id_url = api_get_session_id_url + "?"
        api_get_session_id_url = api_get_session_id_url + urlencode(api_get_session_id_payload)

        api_get_session_id_response = requests_session.post(api_get_session_id_url)
        api_get_session_id_obj = api_get_session_id_response.json()
        token = api_get_session_id_obj['query']['tokens']['logintoken']
        pprint(api_get_session_id_obj)

        # https://www.mediawiki.org/wiki/API:Login
        api_do_login_payload = {
            "action": "login",
            "lgname": config_file_obj['wikiuser']['username'],
            "lgpassword": config_file_obj['wikiuser']['password'],
            "lgtoken" : token,
            "format": "json"
        }
        api_do_login_url = "https://{}.{}/api.php".format(lang, dns_domain)

        api_do_login_response = requests_session.post(api_do_login_url, data = api_do_login_payload)
        api_do_login_obj = api_do_login_response.json()
        pprint(api_do_login_obj)

        with open(session_file,"w") as fh:
            yaml.dump(requests.utils.dict_from_cookiejar(requests_session.cookies), fh, default_flow_style=False)
            fh.close()
    return session_file

def make_api_mw(lang, requests_session, payload, api_format="json"):
    payload_copy = dict(payload)
    payload_copy["format"] = api_format

    api_url = "https://{}.{}/api.php".format(lang, dns_domain)
    api_url = api_url + "?"
    api_url = api_url + urlencode(payload_copy)

    api_response = requests_session.get(api_url)

    if api_format == "json":
        api_obj = api_response.json()
    else:
        api_obj = api_response.content
    return api_obj

def get_requests_session(session_file):
    requests_session = requests.Session()
    with open(session_file) as fh:
        data_from_file = yaml.load(fh)
        session = requests_session.cookies=requests.utils.cookiejar_from_dict(data_from_file)
        fh.close()
    return requests_session

def make_api_commons(payload, api_format="json"):
    payload_copy = dict(payload)
    payload_copy["format"] = api_format

    api_url = "https://commons.wikimedia.org/w/api.php"
    api_url = api_url + "?"
    api_url = api_url + urlencode(payload_copy)

    api_response = requests.get(api_url)

    if api_format == "json":
        api_obj = api_response.json()
    else:
        api_obj = api_response.content
    return api_obj

templates_whitelist = []
if lang == 'it':
    templates_whitelist = [
     'BeginExercise',
     'BeginLemma',
     'BeginProof',
     'BeginRemark',
     'BeginTheorem',
     'EndExercise',
     'EndLemma',
     'EndProof',
     'EndRemark',
     'EndTheorem',
     'FineAssioma',
     'FineCorollario',
     'FineDefinizione',
     'FineDimostrazione',
     'FineEsempio',
     'FineEsercizio',
     'FineLemma',
     'FineOsservazione',
     'FineProposizione',
     'FineTeorema',
     'InizioAssioma',
     'InizioCorollario',
     'InizioDefinizione',
     'InizioDimostrazione',
     'InizioEsempio',
     'InizioEsercizio',
     'InizioLemma',
     'InizioOsservazione',
     'InizioProposizione',
     'InizioTeorema',

     'Center',
     'Infobox',
     'DeleteMe',
    ]
