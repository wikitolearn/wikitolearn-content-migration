#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

import requests_cache
requests_cache.install_cache()

pool_files = destination_link['shared']['files']

files = pool_files.find(
    {
        'revisions':{'$exists':False} # HACK
    }
)
not_found_files = list()

session_file = login_api(lang, dns_domain)
requests_session = get_requests_session(session_file)

for file_entry in tqdm(files, total=files.count()):
    source = file_entry['source']
    api_params = {
        'action' : 'query',
        'prop' : 'imageinfo',
        'iiprop' : '|'.join([
            'url',
            'timestamp',
            'user',
            'userid',
        ]),
        'iilimit' : 'max',
        'titles' : 'File:{}'.format(file_entry['filename']),
    }
    if source != 'commons':
        result = make_api_mw(source, requests_session, api_params)
    else:
        result = make_api_commons(api_params)
    for page_id in result['query']['pages']:
        if 'imageinfo' in result['query']['pages'][page_id]:
            revisions = result['query']['pages'][page_id]['imageinfo']
            for rev in revisions:
                rev['timestamp'] = datetime.datetime.strptime(rev['timestamp'], "%Y-%m-%dT%H:%M:%SZ")
                rev['page_id'] = page_id
                pool_files.update_one({'_id':file_entry['_id']},{'$set':{'revisions' : revisions}})
        else:
            not_found_files.append(file_entry)
            pool_files.update_one({'_id':file_entry['_id']},{'$set':{'revisions' : None}})

print('Files not found: {}\n'.format(len(not_found_files)))
pprint(not_found_files)
