#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

import requests_cache
requests_cache.install_cache()

pool_files = destination_link['shared']['files']

files = pool_files.find()

session_file = login_api(lang, dns_domain)
requests_session = get_requests_session(session_file)

for file_entry in tqdm(files, total=files.count()):
    missing = 0
    for file_revision in file_entry['revisions']:
        download_request = requests.get(file_revision['url'])
        content_type = download_request.headers.get('Content-Type')
        if content_type == 'image/jpeg':
            file_revision['destination'] = "image"
        elif content_type == 'image/png':
            file_revision['destination'] = "image"
        elif content_type == 'image/svg+xml':
            file_revision['destination'] = "image"
        elif content_type == 'image/gif':
            file_revision['destination'] = "image"
        elif content_type == 'application/pdf':
            file_revision['destination'] = "file"
        elif content_type == 'text/plain; charset=UTF-8':
            logger.warn("Missing file revision: '%s': %s", file_revision['url'], content_type)
        else:
            missing += 1
            logger.warn("File: '%s': %s", file_revision['url'], content_type)
        if 'destination' in file_revision:
            file_revision['data'] = download_request.content

    if missing == len(file_entry['revisions']):
        logger.error("failed: %s '%s'", file_entry.get('source'), requests.utils.quote(file_entry.get('filename')))
    pool_files.update_one({
        '_id': file_entry['_id'],
    },{
        '$set':file_entry,
    })
