#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

mw_pages_collection = destination_db['mw_pages']
all_pages = mw_pages_collection.find()

for page in tqdm(all_pages, total=all_pages.count()):
    sql = ""
    sql += "SELECT "
    sql += " * "
    sql += " FROM wtl1_superlog "
    sql += " WHERE "
    sql += " ( "
    sql += "    page_id = {} ".format(int(page['page_id']))
    sql += "    AND "
    sql += "    '{}' <= timestamp ".format(page['timestamp'])
    sql += "    AND "
    sql += "    timestamp < '{}' ".format(page['timestamp_next'])
    sql += " ) "
    sql += " ORDER BY timestamp ASC,log_id ASC,rev_id ASC"

    cursor = source_db.cursor()
    cursor.execute(sql)
    superlog = mysql_fetchall(cursor)
    user_ids = []
    millis_to_make_the_ordering_strong = 0
    for superlog_entry in superlog:
        if superlog_entry['type'] == None:
            superlog_entry['timestamp'] += datetime.timedelta(seconds=millis_to_make_the_ordering_strong)
            user_ids += [superlog_entry['user_id']]
        else:
            superlog_entry['timestamp'] += datetime.timedelta(seconds=millis_to_make_the_ordering_strong)
        millis_to_make_the_ordering_strong += 1
    page['superlog'] = superlog
    mw_pages_collection.update_one({'_id':page['_id']}, {'$set':page})
