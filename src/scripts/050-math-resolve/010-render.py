#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *
sys.exit(0)
pool_formulas = destination_aux_db['formulas']

# pool_formulas.update_many({'$and':[
#     {'formula_type': {'$ne': 'formula'}},
#     {'formula_type': {'$ne': 'formula_texvcjs'}},
#     {'formula_type': {'$ne': 'online'}}, # HACK
#     {'formula_type': {'$exists': True}},
# ]},{
#     '$unset':{
#         'formula_type':1,
#     },
# })

formulas = pool_formulas.find({
    'formula_type':{
        '$exists':False,
    }
})

eve_base_url_formulas = os.environ.get("MATH_BACKEND_URI") + "/v1/formulas"

def online_check_formula(tex):
    restbase_base_url = "https://restbase.wikitolearn.org/it.wikitolearn.org"
    url_check = restbase_base_url + '/v1/media/math/check/tex'
    header= {'Accept':'application/json',
             'Content-Type': 'application/x-www-form-urlencoded'}
    payload = {'q': tex}
    r = requests.post(url_check, data=payload, headers = header)
    if r.status_code == 200:
        loc = r.headers['x-resource-location']
        url_render = restbase_base_url + '/v1/media/math/render/'

        headers_svg= {'Accept':'image/svg+xml',
                 'Content-Type': 'application/x-www-form-urlencoded'}
        r_svg = requests.get(url_render+"svg/"+loc, headers = headers_svg)
        if r_svg.status_code != 200:
            raise ValueError("svg failed")

        headers_mml= {'Accept':'image/mathml+xml',
        'Content-Type': 'application/x-www-form-urlencoded'}
        r_mathml = requests.get(url_render+"mml/"+loc, headers = headers_mml)
        if r_svg.status_code != 200:
            raise ValueError("mml failed")
    else:
        raise ValueError("check failed")
    return True

def worker_fn(formula_object):
    testing = True
    if 'formula_type' in formula_object:
        for formula_type in ['formula', 'formula_texvcjs']:
            formula = formula_object[formula_type]
            if testing and formula != None:
                try:
                    formula_hash = hashlib.sha256(formula.encode('utf-8')).hexdigest()
                    if requests.get(eve_base_url_formulas + "/" + formula_hash).status_code == 404:
                        requests.post(
                            eve_base_url_formulas,
                            data={
                                "formula": formula
                            }
                        ).raise_for_status()
                    formula_object['formula_type'] = formula_type
                    formula_object['formula_hash'] = formula_hash

                    testing = False
                except Exception as exc:
                    logger.debug("{} {}: {}".format(formula_object['_id'],formula_type, exc))
        if not 'formula_type' in formula_object:
            try:
                online_check_formula(formula_object['formula'])
                formula_object['formula_type'] = 'online'
            except Exception as exc:
                logger.debug("Online check failed {}".format(formula_object['_id']))
                formula_object['formula_type'] = None
    return (formula_object['_id'], formula_object)

multiprocessing_pool = multiprocessing.Pool(multiprocessing.cpu_count())

for formula_id_to_formula_object in tqdm(multiprocessing_pool.imap(worker_fn, formulas), total=formulas.count()):
    (formula_id, formula_object) = formula_id_to_formula_object
    pool_formulas.update_one({
        '_id': formula_id,
    }, {
        '$set': formula_object,
    })
