#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

if requests.get(keycloak_realm_base_url,headers=keycloak_get_auth_headers(),verify=keycloak_requests_verify).status_code == 404:
    print("create realm")
    create_realm = requests.post(
        keycloak_base_url + "admin/realms",
        json={
            'enabled': True,
            'id': keycloak_realm_id,
            'realm': keycloak_realm_id
        },
        headers=keycloak_get_auth_headers(),
        verify=keycloak_requests_verify
    )
    create_realm.raise_for_status()

has_client_frontend = False

clients_list_request = requests.get(keycloak_realm_base_url + "/clients",headers=keycloak_get_auth_headers(),verify=keycloak_requests_verify)
for client in clients_list_request.json():
    has_client_frontend = has_client_frontend or (client.get('clientId') == "frontend")

if not has_client_frontend:
    print("create client frontend")
    create_frontend_client = requests.post(
        keycloak_realm_base_url + "/clients",
        json={
            "enabled":True,
            "attributes":{},
            "redirectUris":[],
            "clientId":"frontend",
            "rootUrl":"http://localhost:13000",
            "protocol":"openid-connect"
        },
        headers=keycloak_get_auth_headers(),
        verify=keycloak_requests_verify
    )
    create_frontend_client.raise_for_status()

keep_delete = True
while keep_delete:
    keep_delete = False
    users_url = keycloak_realm_base_url + "/users?max=500"
    all_keycloak_users = requests.get(users_url,headers=keycloak_get_auth_headers(),verify=keycloak_requests_verify)
    all_keycloak_users.raise_for_status()
    for keycloak_uesr in tqdm(all_keycloak_users.json()):
        current_user_url = keycloak_realm_base_url + "/users/" + keycloak_uesr.get('id')
        keep_delete = True

        requests.delete(
            current_user_url,
            headers=keycloak_get_auth_headers(),
            verify=keycloak_requests_verify,
        ).raise_for_status()
