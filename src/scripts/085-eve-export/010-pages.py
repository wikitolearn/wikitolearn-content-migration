#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

def wtl_parse_mw_text(wikitext):
    parsoid_output = parse_mw_text(wikitext)
    if parsoid_output == "":
        return ""

    parsoid_hash = hashlib.sha256(parsoid_output.encode('utf-8')).hexdigest()

    cache = destination_aux_db['wtl_parsed_cache'].find_one({'$or':[
        {'parsoid_hash':parsoid_hash},
    ]}, sort=[("parsoid_hash", 1)])
    if cache == None or not 'html' in cache:
        logger.debug("Cache MISS for wtl html parsing {}".format(parsoid_hash))

        bs_obj = BeautifulSoup(parsoid_output, 'lxml')
        bs_body = bs_obj.find('body')
        for k in list(bs_body.attrs.keys()):
            del bs_body.attrs[k]

        return_html = bs_body.prettify().strip()[6:-7]
    else:
        return_html = cache['html']

    if cache == None:
        destination_aux_db['wtl_parsed_cache'].insert_one(
            {
                'parsoid_hash':parsoid_hash,
                'html':return_html,
            }
        )
    else:
        destination_aux_db['wtl_parsed_cache'].update_one({
            '_id':cache['_id'],
        }, {'$set':{
            "html":return_html,
        }})
    return return_html

wtl_revisions_collection = destination_db['wtl_revisions']

page_ids = wtl_revisions_collection.find({
    'category': 'CourseLevelThree',
    'to_eve': 1,
}).distinct("mw_page_id")

eve_base_url_pages = os.environ.get("PAGES_BACKEND_URI") + "/v1/"

for page_id in tqdm(page_ids):
    eve_id = None
    eve_etag = None
    eve_version = None
    all_revisions = wtl_revisions_collection.find({
        'mw_page_id':page_id,
    }).sort("revision_number",pymongo.ASCENDING)

    for revision in tqdm(all_revisions, total=all_revisions.count()):
        if 'eve' in revision:
            eve_id = revision['eve']['id']
            eve_etag = revision['eve']['etag']
            eve_version = revision['eve']['version']
        else:
            eve_revision_payload = {}
            eve_revision_payload['title'] = revision['page_title']
            eve_revision_payload['content'] = wtl_parse_mw_text(revision['payload']['page_wikitext'])
            eve_revision_payload['authors'] = revision['user_ids']
            eve_revision_payload['language'] = lang
            if eve_id == None or eve_etag == None:
                create_reply = requests.post(
                    url="{}pages".format(eve_base_url_pages),
                    json=eve_revision_payload
                )
                create_reply.raise_for_status()
                create_obj = create_reply.json()
                eve_id = create_obj['_id']
                eve_etag = create_obj['_etag']
                eve_version = create_obj['_version']
            else:
                update_reply = requests.patch(
                    url="{}pages/{}".format(eve_base_url_pages, eve_id),
                    json=eve_revision_payload,
                    headers={'If-Match': eve_etag}
                )
                update_reply.raise_for_status()
                update_obj = update_reply.json()
                eve_id = update_obj['_id']
                eve_etag = update_obj['_etag']
                eve_version = update_obj['_version']
            wtl_revisions_collection.update_one({
                '_id':revision['_id'],
            }, {'$set' :{
                'eve': {
                    'id': eve_id,
                    'etag': eve_etag,
                    'version': eve_version,
                },
            }})
