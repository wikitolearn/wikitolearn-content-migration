#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

mw_pages_collection = destination_db['mw_pages']

lv3_missing_pages = []


all_lv2_ids = mw_pages_collection.find({
    'category':"CourseRoot"
}).distinct("superlog.sub_pages.id_list")

for lv2_id in tqdm(all_lv2_ids):
    page = mw_pages_collection.find_one({'_id':lv2_id})

    new_superlog = []
    for superlog_entry in tqdm(page['superlog']):
        level_two_text = superlog_entry['page_wikitext']

        regex = r"\*\s*\[{2}(?P<full_page_name>[^\]]*)?(\|(?P<title>[^\]]*))\]{2}\s*"
        matches = re.finditer(regex, level_two_text)

        sub_pages = []
        sub_pages_titles = []
        for match in matches:
            page_title = None
            title_from_full_name = match.groupdict()['full_page_name'].rsplit('/', 1)[1]
            title_from_link = match.groupdict()['title']
            if title_from_full_name.replace(' ', '_').strip() != title_from_link.replace(' ', '_').strip():
                if title_from_link.count('/') == 1:
                    page_title = title_from_link.strip()
                elif title_from_full_name != 'Z[x]':
                    page_title = title_from_full_name.strip()
            else:
                page_title = title_from_full_name.strip()

            if page_title == "La misura di <math>\\rho </math>: il problema della materia oscura":
                page_title = "La_misura_di_\\rho_:_il_problema_della_materia_oscura"

            sub_pages_titles.append(page_title)
            sub_pages.append(lookup_sub_page(mw_pages_collection, lang, page_title, page, lv3_missing_pages))

        superlog_entry['sub_pages_titles'] = sub_pages_titles
        superlog_entry['sub_pages'] = sub_pages
        new_superlog.append(superlog_entry)
    mw_pages_collection.update_many({'_id':page['_id']},{'$set':{
        'superlog':new_superlog,
        'category':'CourseLevelTwo',
        'page_in_use':True,
    }})

asset_filename = "/opt/assets/" + lang + "/lv3_missing_pages.yml"
if os.path.isfile(asset_filename):
    with open(asset_filename) as fh:
        lv3_missing_from_file = yaml.load(fh)
        if lv3_missing_from_file != None:
            for l in lv3_missing_from_file:
                if l in lv3_missing_pages:
                    lv3_missing_pages.remove(l)

if len(lv3_missing_pages) > 0:
    print("\n\nError!")
    lv3_missing_pages = sorted(lv3_missing_pages, key=lambda k: k['sub_page_title'])
    print(yaml.dump(lv3_missing_pages, default_flow_style=False))
    sys.exit(1)
