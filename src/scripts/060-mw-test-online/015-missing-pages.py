#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

mw_pages_collection = destination_db['mw_pages']

all_pages = mw_pages_collection.find({
    'category': {'$in':[
        'CourseRoot',
        'CourseLevelTwo',
    ]},
})

missing_sub_level_in_edge = []

for page in tqdm(all_pages, total=all_pages.count()):
    sub_pages = page['superlog'][len(page['superlog'])-1]['sub_pages']
    for sub_page in sub_pages:
        if sub_page['id'] == None:
            if not page in missing_sub_level_in_edge:
                missing_sub_level_in_edge.append(page)

if len(missing_sub_level_in_edge) > 0:
    for page in missing_sub_level_in_edge:
        last_log = page['superlog'][len(page['superlog'])-1]
        if last_log['page_namespace'] == 2800:
            mw_title = "Course:" + last_log['page_title']
        if last_log['page_namespace'] == 2:
            mw_title = "User:" + last_log['page_title']
        logger.error("https://" + lang + ".wikitolearn.org/" + mw_title)
