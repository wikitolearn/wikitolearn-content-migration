#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
sys.exit(0) # FIXME
from commons import *

mw_session = get_requests_session(login_api(lang, dns_domain))
mw_pages_collection = destination_db['mw_pages']

all_pages = mw_pages_collection.find({
    'category': 'CourseRoot',
})

for page in tqdm(all_pages, total=all_pages.count()):
    last_log = page['superlog'][len(page['superlog'])-1]
    payload = {
        'action': 'coursetree',
    }
    if last_log['page_namespace'] == 2800:
        payload['coursetitle'] = "Course:" + last_log['page_title']
    if last_log['page_namespace'] == 2:
        payload['coursetitle'] = "User:" + last_log['page_title']

    api_response = make_api_mw(lang, mw_session, payload)
    api_lv2 = api_response['coursetree']['response']['levelsTwo']
    api_lv3 = api_response['coursetree']['response']['levelsThree']

    for i in range(len(api_lv2)):
        api_lv2[i] = api_lv2[i].strip()

    mig_lv2 = last_log['sub_pages_titles']
    if len(api_lv2) != len(mig_lv2):
        raise ValueError(str(page['_id']))
    count_lv2 = len(mig_lv2)
    for index_value in range(count_lv2):
        if mig_lv2[index_value] != api_lv2[index_value]:
            raise ValueError(str(page['_id']))
        api_lv3_list = api_lv3[index_value]
        if api_lv3_list == ['']: # HACK
            api_lv3_list = []
        mig_lv3_object = mw_pages_collection.find_one({
            '_id': last_log['sub_pages'][index_value]['id'],
        })
        if mig_lv3_object == None and len(api_lv3_list) > 0:
            raise ValueError(str(page['_id']))
        elif mig_lv3_object == None and len(api_lv3_list) == 0:
            pass
        else:
            last_lv3_superlog = mig_lv3_object['superlog'][len(mig_lv3_object['superlog'])-1]['sub_pages_titles']

            if len(last_lv3_superlog) != len(api_lv3_list):
                pprint(last_lv3_superlog)
                pprint(api_lv3_list)
                raise ValueError(str(page['_id']))
            for inside_index_value in range(len(last_lv3_superlog)):
                if api_lv3_list[inside_index_value] == "La misura di <math>\\rho </math>: il problema della materia oscura":
                    api_lv3_list[inside_index_value] = "La_misura_di_\\rho_:_il_problema_della_materia_oscura"
                if last_lv3_superlog[inside_index_value].strip().replace('_', ' ') != api_lv3_list[inside_index_value].strip().replace('_', ' '):
                    pprint(last_lv3_superlog[inside_index_value].encode('utf8'))
                    pprint(api_lv3_list[inside_index_value].encode('utf8'))
                    raise ValueError(str(page['_id']))
