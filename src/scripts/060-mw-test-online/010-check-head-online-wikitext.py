#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
sys.exit(0) # FIXME
from commons import *

mw_session = get_requests_session(login_api(lang, dns_domain))
mw_pages_collection = destination_db['mw_pages']

all_pages = mw_pages_collection.find({
    'category': 'CourseLevelThree',
}) # .skip(2000) # .limit(10)

for page in tqdm(all_pages, total=all_pages.count()):
    last_log = page['superlog'][len(page['superlog'])-1]

    payload = {
        'action': "query",
        'format': "json",
        'prop': "revisions",
        'rvprop': "content",
    }
    if last_log['page_namespace'] == 2800:
        payload['titles'] = "Course:" + last_log['page_title']
    if last_log['page_namespace'] == 2:
        payload['titles'] = "User:" + last_log['page_title']

    api_response = make_api_mw(lang, mw_session, payload)
    try:
        api_mw_text = api_response['query']['pages'][str(page['page_id'])]['revisions'][0]['*'].strip()
        api_mw_text = api_mw_text.replace(dns_domain, "wikitolearn.org")
        mig_mw_text = last_log['page_wikitext'].strip()
        mig_mw_text = mig_mw_text.replace(dns_domain, "wikitolearn.org")
        if api_mw_text != mig_mw_text:
            raise ValueError()
    except Exception as e:
        print(page['_id'])
        print(page['page_title'])
        raise ValueError(page['_id'])
