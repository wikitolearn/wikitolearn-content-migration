#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

sql = ""
sql += "SELECT "
sql += " * "
sql += " FROM wtl1_unified_revision "
sql += " LIMIT 0,1 "

cursor = source_db.cursor()
cursor.execute(sql)
wtl1_unified_revision = mysql_fetchall(cursor)

sql = ""
sql += "SELECT "
sql += " * "
sql += " FROM wtl1_logging "
sql += " LIMIT 0,1 "

cursor = source_db.cursor()
cursor.execute(sql)
wtl1_logging = mysql_fetchall(cursor)

wtl1_unified_revision_keys = {'page_title', 'page_id', 'user_id', 'is_redirect', 'rev_parent_id', 'rev_id', 'timestamp', 'page_namespace', 'rev_text_id'}
wtl1_logging_keys  = {'type', 'page_title', 'page_id', 'user_id', 'action', 'timestamp', 'page_namespace', 'params', 'log_id'}


columns = []
for l in wtl1_unified_revision_keys:
    if not l in columns:
        columns.append(l)
for l in wtl1_logging_keys:
    if not l in columns:
        columns.append(l)

columns = sorted(columns)

sql = ""
sql += "SELECT "
for l in columns:
    if columns.index(l) > 0:
        sql += " , "
    if l in wtl1_unified_revision_keys:
        sql += l
    else:
        sql += " NULL AS {} ".format(l)
sql += " FROM wtl1_unified_revision "

sql += " UNION "

sql += "SELECT "
for l in columns:
    if columns.index(l) > 0:
        sql += " , "
    if l in wtl1_logging_keys:
        sql += l
    else:
        sql += " NULL AS {} ".format(l)
sql += " FROM wtl1_logging "

sql_filter = ""
sql_filter += " WHERE "
sql_filter += " NOT ( "
sql_filter += "  type = 'usermerge' "
sql_filter += "  OR "
sql_filter += "  type = 'review' "
sql_filter += "  OR "
sql_filter += "  type = 'liquidthreads' "
sql_filter += "  OR "
sql_filter += "  type = 'patrol' "
sql_filter += "  OR "
sql_filter += "  type = 'rights' "
sql_filter += "  OR "
sql_filter += "  type = 'protect' "
sql_filter += "  OR "
sql_filter += "  type = 'import' "
sql_filter += "  OR "
sql_filter += "  type = 'lock' "
sql_filter += "  OR "
sql_filter += "  type = 'block' "
sql_filter += "  OR "
sql_filter += "  type = 'upload' "
sql_filter += "  OR "
sql_filter += "  type = 'newusers' "
sql_filter += "  OR "
sql_filter += "  type = 'spamblacklist' "
sql_filter += "  OR "
sql_filter += "  (type = 'delete' AND action = 'flow-delete-topic') "
sql_filter += "  OR "
sql_filter += "  (type = 'delete' AND action = 'flow-delete-post') "
sql_filter += " ) "
sql_filter += " OR "
sql_filter += " type IS NULL "

tab = "wtl1_superlog"

source_db.query("DROP TABLE IF EXISTS " + tab)
source_db.query("CREATE TABLE " + tab + " AS " + sql + sql_filter)
source_db.query("ALTER TABLE " + tab + " ADD INDEX(timestamp);")
source_db.query("ALTER TABLE " + tab + " ADD INDEX(page_id);")
source_db.query("ALTER TABLE " + tab + " ADD INDEX(page_title);")
source_db.query("ALTER TABLE " + tab + " ADD INDEX(rev_parent_id);")
