#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

sql = ""
sql += " SELECT "
sql += " log_id "
sql += " , "
sql += " log_type AS type "
sql += " , "
sql += " log_action AS action "
sql += " , "
sql += " STR_TO_DATE(log_timestamp, '%Y%m%d%H%i%s') AS timestamp "
sql += " , "
sql += " log_user AS user_id"
sql += " , "
sql += " log_namespace AS page_namespace "
sql += " , "
sql += "  CAST(log_title AS binary) AS page_title "
sql += " , "
sql += " log_page AS page_id "
sql += " , "
sql += " log_params AS params " # FIXME maybe need a cast?
sql += " FROM logging "

tab = "wtl1_logging"

source_db.query("DROP TABLE IF EXISTS " + tab)
source_db.query("CREATE TABLE " + tab + " AS " + sql)
source_db.query("ALTER TABLE " + tab + " ADD INDEX(timestamp);")
source_db.query("ALTER TABLE " + tab + " ADD INDEX(page_id);")
