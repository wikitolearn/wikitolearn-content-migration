#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

mw_users_collection = destination_link['shared']['users']

source_shared_db = MySQLdb.connect('source', 'root', 'root', 'shared'+"wikitolearn", use_unicode=True, charset="utf8mb4")

wtl_revisions_collection = destination_db['wtl_revisions']

course_root_revisions = wtl_revisions_collection.find({
    'category': 'CourseRoot',
})

for course_root_revision in tqdm(course_root_revisions, total=course_root_revisions.count()):
    mw_userid_course_owner = None
    userid_course_owner = None
    if course_root_revision['payload']['mw_username_course_owner'] != None:
        sql = "SELECT user_id AS mw_user_id FROM user WHERE CAST(user_name AS binary)  = %s "
        cursor = source_shared_db.cursor()
        cursor.execute(sql, ( course_root_revision['payload']['mw_username_course_owner'], ))
        mw_user_id_list = mysql_fetchall(cursor)
        if len(mw_user_id_list) == 1:
            mw_userid_course_owner = mw_user_id_list[0]['mw_user_id']
            mongodb_search_for_uesr = mw_users_collection.find({
                'mw_user_id':int(mw_user_id_list[0]['mw_user_id']),
            })
            if mongodb_search_for_uesr.count() != 1:
                raise ValueError("Missing {}".format(int(mw_user_id_list[0]['mw_user_id'])))
            for mongodb_user_object in mongodb_search_for_uesr:
                if not 'keycloak_id' in mongodb_user_object:
                    logging.error("Missing keycloak_id for {}".format(int(mw_user_id_list[0]['mw_user_id'])))
                else:
                    userid_course_owner = mongodb_user_object['keycloak_id']

        else:
            print("\n\n")
            print(course_root_revision['payload']['mw_username_course_owner'])
            print("\n\n")
            raise ValueError()

    wtl_revisions_collection.update_one({
        '_id':course_root_revision['_id'],
    }, {'$set': {
        'payload.mw_userid_course_owner': mw_userid_course_owner,
        'payload.userid_course_owner': userid_course_owner,
    }})
