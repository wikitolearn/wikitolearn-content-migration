#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

mw_users_collection = destination_link['shared']['users']

mw_pages_collection = destination_db['mw_pages']
wtl_revisions_collection = destination_db['wtl_revisions']
wtl_revisions_collection.drop()
wtl_revisions_collection.create_index("payload.subpages")
wtl_revisions_collection.create_index("user_ids")
wtl_revisions_collection.create_index("mw_page_id")
wtl_revisions_collection.create_index("timestamp")
wtl_revisions_collection.create_index("deleted")
wtl_revisions_collection.create_index("category")
wtl_revisions_collection.create_index("page_title")

empty_courses = []
all_courses = mw_pages_collection.find({'category': 'CourseRoot'})
for page in tqdm(all_courses, total=all_courses.count()):
    if len(page['superlog'][-1]['sub_pages']) == 0:
        if len(page['superlog']) == 1:
            empty_courses.append(page['_id'])
pprint(empty_courses)
all_pages = mw_pages_collection.find({'_id': {"$nin": empty_courses}})

for page in tqdm(all_pages, total=all_pages.count()):
    for att_to_delete in [
            'page_id',
            'rev_id',
            'page_namespace',
            'page_title',
            'all_used_titles',
            'user_ids',
            'timestamp',
            'timestamp_next',
        ]:
        if att_to_delete in page:
            del page[att_to_delete]

    superlog_index = 0
    for superlog_entry in page['superlog']:
        superlog_entry['category'] = page['category']
        superlog_entry['mw_page_id'] = page['_id']
        superlog_entry['private'] = superlog_entry['page_namespace'] == 2

        for att_to_delete in [
                'previous_page_namespace',
                'previous_page_title',
                'is_redirect',
                'rev_parent_id',
                'rev_text_id',
                'params',
                'log_id',
                'page_id',
                'page_namespace',
                'rev_id',
                'type',
                'action',
                'params_move',
                'sub_pages_titles',
                'timestamp_next',
                'all_users_ids',
            ]:
            if att_to_delete in superlog_entry:
                del superlog_entry[att_to_delete]


        user_ids = []
        if 'user_ids' in superlog_entry:
            user_ids = superlog_entry['user_ids']

        if 'user_id' in superlog_entry:
            user_ids.append(superlog_entry['user_id'])
            del superlog_entry['user_id']

        superlog_entry['mw_user_ids'] = sorted(list(set(user_ids)))
        superlog_entry['user_ids'] = []
        for mw_user_id in  sorted(list(set(user_ids))):
            if mw_user_id in [
                    0,
                    2394, # "move page script"
                ]:
                pass
            else:
                mongodb_search_for_uesr = mw_users_collection.find({
                    'mw_user_id':mw_user_id,
                })
                if mongodb_search_for_uesr.count() != 1:
                    raise ValueError("Missing {}".format(mw_user_id))
                for mongodb_user_object in mongodb_search_for_uesr:
                    if not 'keycloak_id' in mongodb_user_object:
                        logging.error("Missing keycloak_id for {}".format(mw_user_id))
                    else:
                        superlog_entry['user_ids'].append(mongodb_user_object['keycloak_id'])

        superlog_entry['revision_number'] = superlog_index
        subpages = []
        if 'sub_pages' in superlog_entry:
            for sub_page in superlog_entry['sub_pages']:
                subpages.append({'id':sub_page['id']})
            del superlog_entry['sub_pages']
            superlog_entry['payload'] ={
                'subpages': subpages,
            }
        else:
            superlog_entry['payload'] ={
                'page_wikitext': superlog_entry['page_wikitext'],
            }

        if 'page_html' in superlog_entry:
            del superlog_entry['page_html']
        if 'page_wikitext' in superlog_entry:
            del superlog_entry['page_wikitext']
        wtl_revisions_collection.insert(superlog_entry)

        superlog_index += 1
