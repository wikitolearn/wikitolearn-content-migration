#!/bin/bash
if test ! -f "${BASH_SOURCE[0]}"
then
  echo "Error getting the script name"
  return
  exit
fi

pathadd () {
  if ! echo "$PATH" | grep -Eq "(^|:)$1($|:)"
  then
    PATH="$PATH:$1"
  fi
}

cd `dirname "${BASH_SOURCE[0]}"`"/bin/"
pathadd `pwd`
cd $OLDPWD
